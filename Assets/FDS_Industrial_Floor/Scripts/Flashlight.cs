﻿using UnityEngine;
using System.Collections;

public class Flashlight : MonoBehaviour {

    Light flashlight;
    [SerializeField]
    private SteamVR_TrackedController VRController;

    void Awake()
    {
        flashlight = GetComponent<Light>();
    }

	void Update () {
        if (VRController.triggerPressed)
        {
            flashlight.enabled = true;
        }
        else
            flashlight.enabled = false;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : InteractableObject {

    private bool isClosed;

    [SerializeField]
    private bool openOutward;

    [SerializeField]
    private float openAmount;

    [SerializeField]
    private float openTime;

    private Quaternion targetRot;
	// Use this for initialization
	void Start () {
        isClosed = true;
        
	}
	
	// Update is called once per frame
	void Update () {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(transform.rotation.eulerAngles.x, targetRot.eulerAngles.y, transform.rotation.eulerAngles.z), openTime * Time.deltaTime);

	}
    public override void RunInteraction()
    {
        Debug.Log("Interact");
        float mlt = 1;
        if(openOutward)
        {
            mlt = -1;
        }
        if (isClosed)
        {
            targetRot = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + openAmount*mlt, transform.rotation.eulerAngles.z);
            isClosed = false;
        }
        else
        {
            targetRot = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - openAmount*mlt, transform.rotation.eulerAngles.z);
            isClosed = true;
        }

    }
}

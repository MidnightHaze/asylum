﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace VSControl
{
    public class HandController : MonoBehaviour
    {

        [SerializeField]
        private SteamVR_TrackedController VRController;
        [SerializeField]
        private float ReworkTimer;

        private float coolDown;
        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if (VRController.triggerPressed && coolDown<=0)
            {
                coolDown = ReworkTimer;
                RaycastHit hitInfo;
                Physics.SphereCast(transform.position, 0.3f, transform.forward, out hitInfo,
                               1f);
                Debug.Log("CAST:" + hitInfo.collider.name);
                if(hitInfo.collider != null)
                    if(hitInfo.transform.gameObject.GetComponent<InteractableObject>() != null)
                    {
                        hitInfo.transform.gameObject.GetComponent<InteractableObject>().RunInteraction();
                    }

            }
            if (coolDown > 0)
                coolDown -= Time.deltaTime;
        }
    }
}